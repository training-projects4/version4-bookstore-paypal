import express from 'express';
import featuredData from './featuredData.js';

const app = express();

app.get('/api/products', (req, res) => {
  res.send(featuredData.products);
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`serve at http://localhost:${port}`);
}); 