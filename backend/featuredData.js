const data = {
    products: [
      {
        id:1,
        name: 'Java',
        category: 'Programming',
        image: '/images/products/java.jpg', // 679px × 829px
        price: 269.99,
        countInStock: 10,
        rating: 4.5,
        numReviews: 10,
        description: 'The best way to learn Java is by doing. This book includes a unique project at the end of the book that requires the application of all the concepts taught previously. Working through the project will not only give you an immense sense of achievement, it will also help you retain the knowledge and master the language.',
      },
      {
        id:2,
        name: 'Python',
        category: 'Programming',
        image: '/images/products/python.png', // 679px × 829px
        price: 259.99,
        countInStock: 10,
        rating: 4.5,
        numReviews: 10,
        description: `"Start to learn the hard core of python computer programming, python data analysis, and python coding projects. The contents of this eBook is simple, yet detailed enough to turn you the python bravura, no matter your field. Click the button below to discover how simple and scintillating python programming can be.",
        `,
      },
      {
        id:3,
        name: 'C++',
        category: 'Programming',
        image: '/images/products/cplus.jpg', // 679px × 829px
        price: 199.99,
        countInStock: 10,
        rating: 4.5,
        numReviews: 10,
        description: `API Design for C++ provides a comprehensive discussion of Application Programming Interface (API) development, from initial design through implementation, testing, documentation, release, versioning, maintenance, and deprecation.`,
      },
      {
        id:4,
        name: 'JavaScript',
        category: 'Programming',
        image: '/images/products/javascript.jpg', 
        price: 249.99,
        countInStock: 10,
        rating: 4.5,
        numReviews: 10,
        description: `This book demonstrates the capabilities of JavaScript for web application development by combining theoretical learning with code exercises and fun projects that you can challenge yourself with. The guiding principle of the book is to show how straightforward JavaScript techniques can be used to make web apps ranging from dynamic websites to simple browser-based games.`,
      },
    ],
  };
  export default data;